﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;


public class PlayMovieCanvas : MonoBehaviour
{

    public bool playOnStart;
    public bool loop;
    MovieTexture movie;

    void Awake()
    {
        RawImage r = GetComponent<RawImage>();
        movie = (MovieTexture)r.mainTexture;
        movie.loop = loop;
    }

    void Update()
    {
        if(playOnStart)
            movie.Play();

     
    }
}