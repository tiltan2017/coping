﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenManager : MonoBehaviour
{


    [SerializeField] string sceneToLoad;
    [SerializeField] float delay;
    [SerializeField] Image fadeImage;
    [SerializeField] float fadeInDelay;
    [SerializeField] float fadeOutDelay;


    // Use this for initialization
    IEnumerator Start ()
	{
	  yield return StartCoroutine(FadeIn());
        yield return new WaitForSeconds(delay);
        yield return StartCoroutine(FadeOut());
        SceneManager.LoadScene(sceneToLoad);


    }

    IEnumerator FadeIn()
    {
        fadeImage.CrossFadeAlpha(0, 0, false);
        fadeImage.CrossFadeAlpha(1, fadeInDelay, false);
        yield return new WaitForSeconds(fadeInDelay);
    }
    IEnumerator FadeOut()
    {
        fadeImage.CrossFadeAlpha(1, 0, false);
        fadeImage.CrossFadeAlpha(0, fadeOutDelay, false);
        yield return new WaitForSeconds(fadeOutDelay);
    }

}
