﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashFade : MonoBehaviour {

    public List<Image> images;
    public string loadLevel;

    public float delayBeforeSpash = .5f;
    public float fadeInTime = 1f;
    public float delayBetweenFades = 1f;
    public float fadeOutTime = 1.5f;
    public float delayBeforeLoadingScene = .25f;


	// Use this for initialization

    IEnumerator Start()
    {
        if (images != null && images.Count!=0)
        {
            //----------------------------------------------------
            images.ForEach(x=>x.canvasRenderer.SetAlpha(0f));
            yield return new WaitForSeconds(delayBeforeSpash);
            //----------------------------------------------------
            FadeIn();
            yield return new WaitForSeconds(fadeInTime);
            //----------------------------------------------------
            yield return new WaitForSeconds(delayBetweenFades);
            //----------------------------------------------------
            FadeOut();
            yield return new WaitForSeconds(fadeOutTime);
            //----------------------------------------------------
            yield return new WaitForSeconds(delayBeforeLoadingScene);
            SceneManager.LoadScene(loadLevel);
            //----------------------------------------------------
        }
    }

    private void FadeIn()
    {
        images.ForEach(x => x.CrossFadeAlpha(1f, fadeInTime, false));
    }
    private void FadeOut()
    {
        images.ForEach(x => x.CrossFadeAlpha(0f, fadeOutTime, false));

    }





}
