﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CutSceneImages : MonoBehaviour
{
    [Header("references")]
    [SerializeField] Image blackScreenImage;
    [SerializeField] Image imageHolder;

    [Space, Header("settings")]
    [SerializeField] float blackScreenFadeIn;
    [SerializeField] float blackScreenFadeOut;
    [SerializeField] float startDelay;
    [SerializeField] float delayBetweenSlides;
    [SerializeField] bool startCoroutineOnStart;

    [Space, Header("slides")]
    [SerializeField] List<CutSceneSlideData> slides;

    [Space, Header("on cut scene finish")]
    [SerializeField] bool loadScene;
    [SerializeField] string sceneName;
    [SerializeField] float delayBeforeLoadScene;

    Coroutine lastCoroutineCall;


	// Use this for initialization
	void Start () {

        if(startCoroutineOnStart)
            StartCoroutine(StartCutScene(InputsSettings.ProgressDialog));

    }

    // Update is called once per frame
    void Update () {



	}


    public IEnumerator StartCutScene(KeyCode progressKey)
    {

        if (lastCoroutineCall != null)
            yield break;

        //show black screen
        blackScreenImage.gameObject.SetActive(true);
        blackScreenImage.CrossFadeAlpha(1f, 0f, false);
        imageHolder.gameObject.SetActive(true);

        for (int i = 0; i < slides.Count; i++)
        {
            var dialogue = slides[i];

            //if sprites are different
            if (imageHolder.sprite != dialogue.imageToShow)
            {
                //change Sprite
                imageHolder.sprite = dialogue.imageToShow;

                //fade out black screen
                if (dialogue.imageToShow != null)
                    yield return StartCoroutine(FadeOutBlackScreen());

            }

            //show text
            yield return StartCoroutine(DialogManager.Instance.ShowMonologueCoroutine(progressKey, dialogue.Dialog));


            //fade in black screen
            if (i < slides.Count - 1 && dialogue.imageToShow != slides[i + 1].imageToShow)
                if (dialogue.imageToShow != null)
                    yield return StartCoroutine(FadeInBlackScreen());

            yield return new WaitForSeconds(delayBetweenSlides);
        }



        yield return StartCoroutine(FadeInBlackScreen());

        lastCoroutineCall = null;

        yield return new WaitForSeconds(delayBeforeLoadScene);
        imageHolder.gameObject.SetActive(false);

        OnCutSceneFinish();
    }

    IEnumerator FadeInBlackScreen()
    {
        blackScreenImage.gameObject.SetActive(true);
        blackScreenImage.CrossFadeAlpha(0f, 0f, false);
        blackScreenImage.CrossFadeAlpha(1f, blackScreenFadeIn, false);
        yield return new WaitForSeconds(blackScreenFadeIn);

    }
    IEnumerator FadeOutBlackScreen()
    {
        blackScreenImage.gameObject.SetActive(true);
        blackScreenImage.CrossFadeAlpha(1f, 0f, false);
        blackScreenImage.CrossFadeAlpha(0, blackScreenFadeOut, false);
        yield return new WaitForSeconds(blackScreenFadeOut);
        blackScreenImage.gameObject.SetActive(false);


    }

    void OnCutSceneFinish()
    {
        if(loadScene)
           SceneManager.LoadScene(sceneName);
    }

}

[Serializable]
class CutSceneSlideData
{
    [TextArea] public string Dialog;
    public Sprite imageToShow;
}