﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;


public  class InteractiveItem : MonoBehaviour, IInteractiveItem
{
    public static string ItemInteratorTag = "Item Interactor";

    #region fields

    [Header("Interactive Item")]
    //-------------------------------------------
    [SerializeField] GameObject guiElements;

    //IInteractiveItem---------------------------
    bool isGuiForcedHidden;
    bool isGuiForcedVisable;
    [SerializeField] bool _isGuiAlwaysVisable;
    [SerializeField] bool _isGuiAlwaysHidden;

    [SerializeField, ReadOnly] protected bool isDestryed;
    [SerializeField] private bool _isInteractionDiabled;

    [SerializeField] ExternalCollider _externalCollider;
    //-------------------------------------------
    private Action _onMainInteraction;
    private Action _onSecondaryInteraction;
    [SerializeField] UnityEvent onMainInteractionEvent;
    [SerializeField] UnityEvent onSecondaryInteractionEvent;


    #endregion

    #region properties

    //IInteractiveItem---------------------------

    GameObject IInteractiveItem.Instance
    {
        get { return gameObject; }
    }

    public bool IsDestryed
    {
        get { return isDestryed; }
        private set { isDestryed = value; }
    }

    public ExternalCollider ExternalCollider
    {
        get { return _externalCollider; }
    }

    public bool IsInteractionDiabled
    {
        get { return _isInteractionDiabled; }
        set { _isInteractionDiabled = value; }
    }

    public ExternalCollider externalCollider
    {
        get { return _externalCollider; }
    }

    public Action OnMainInteraction
    {
        get { return _onMainInteraction; }
        set { _onMainInteraction = value; }
    }

    public Action OnSecondaryInteraction
    {
        get { return _onSecondaryInteraction; }
        set { _onSecondaryInteraction = value; }
    }

    public bool IsGuiVisable
    {
        get
        {
            if (guiElements != null)
                return false;
            return guiElements.activeInHierarchy;
        }

    }

    public bool IsGuiForcedHidden
    {
        get { return isGuiForcedHidden; }
        private set { isGuiForcedHidden = value; }
    }

    public bool IsGuiForcedVisable
    {
        get { return isGuiForcedVisable; }
        private set { isGuiForcedVisable = value; }
    }

    public bool IsGuiAlwaysVisable
    {
        get
        { return _isGuiAlwaysVisable; }
        private set
        {
            _isGuiAlwaysVisable = value;
            _isGuiAlwaysHidden = false;
            ForceShowGui(value);
        }
    }
    public bool IsGuiAlwaysHidden
    {
        get { return _isGuiAlwaysHidden; }
        private set
        {
            _isGuiAlwaysHidden = value;
            _isGuiAlwaysVisable = false;
            ForceHideGui(value);
        }
    }

    //-------------------------------------------

    #endregion

    #region methods

    //unityEvents--------------------------------
    protected virtual void Awake()
    {
        if (ExternalCollider != null)
        {
            externalCollider.onTriggerExit = OnTriggerExit;
            externalCollider.onTriggerEnter = OnTriggerEnter;
        }

        if(_isGuiAlwaysVisable)
            IsGuiAlwaysVisable = _isGuiAlwaysVisable;

        if (_isGuiAlwaysHidden)
            IsGuiAlwaysHidden = _isGuiAlwaysHidden;

        if (_isInteractionDiabled)
            DisableInteraction(true);
    }

    protected virtual void Update()
    {

        if(isDestryed)return;

   

    }


    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.tag == ItemInteratorTag)
        {

            var interactor = other.GetComponent<IInteractor>();
            interactor.AddItem(this);
        }
    }
    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.tag == ItemInteratorTag)
        {

            var interactor = other.GetComponent<IInteractor>();
            interactor.RemoveItem(this);

        }
    }




    //IInteractiveItem---------------------------
    public virtual void HideGUI()
    {
        if(isGuiForcedVisable || isGuiForcedHidden)
            return;

        if (IsInteractionDiabled)
            return;

        HideGuiWithoutConstrains();
    }
    public virtual void ShowGUI()
    {
        if (isGuiForcedVisable || isGuiForcedHidden)
            return;

        if (IsInteractionDiabled)
            return;

        ShowGuiWithoutConstrains();
    }

    public virtual void ForceShowGui(bool state)
    {
        if(!isGuiForcedHidden)
            if (state && !IsGuiForcedVisable)
            {
                IsGuiForcedHidden = false;
                ShowGuiWithoutConstrains();

            }

        IsGuiForcedVisable = state;

    }
    public virtual void ForceHideGui(bool state)
    {
        if(!IsGuiAlwaysVisable)
            if (state && !IsGuiForcedHidden)
            {
                IsGuiForcedVisable = false;
                HideGuiWithoutConstrains();
            }

        IsGuiForcedHidden = state;

    }

    private void ShowGuiWithoutConstrains()
    {
        if (guiElements != null)
            guiElements.SetActive(true);
    }
    private void HideGuiWithoutConstrains()
    {
        if (guiElements != null)
            guiElements.SetActive(false);
    }

    //--------------------

    public void DisableInteraction(bool state)
    {
        IsGuiAlwaysHidden = state;
        IsInteractionDiabled = state;
        ForceHideGui(state);
    }
   
    //--------------------

    public virtual void MainInteraction()
    {
        if(IsInteractionDiabled) return;
        Debug.Log("Main interaction was triggered");

        if(OnMainInteraction != null)
            OnMainInteraction.Invoke();

        if(onMainInteractionEvent!= null)
            onMainInteractionEvent.Invoke();
    }

    public virtual void SeconderyInteraction()
    {
        if (IsInteractionDiabled) return;

        Debug.Log("Secondery interaction was triggered");

        if (OnSecondaryInteraction != null)
            OnSecondaryInteraction.Invoke();

        if (onSecondaryInteractionEvent != null)
            onSecondaryInteractionEvent.Invoke();
    }

    //-------------------------------------------

    
    #endregion


}
