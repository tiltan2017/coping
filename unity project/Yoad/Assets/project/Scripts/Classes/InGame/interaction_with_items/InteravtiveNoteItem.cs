﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InteractiveItem))]
public class InteravtiveNoteItem : MonoBehaviour
{

    [SerializeField, ReadOnly] InteractiveItem interactiveBase;
    [SerializeField] NoteMessage _message;


    public NoteMessage Message
    {
        get { return _message; }
        set { _message = value; }
    }


    void Awake()
    {
        interactiveBase = GetComponent<InteractiveItem>();
        interactiveBase.OnMainInteraction += ()=> NoteMessage.ShowNote(Message);
    }





}
