﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

public class ItemInteractor : MonoBehaviour, IInteractor
{


    #region fields

    //IInteractor-------------------------------
    List<IInteractiveItem> itemsInRange = new List<IInteractiveItem>();
    IInteractiveItem nearestItem;

    [SerializeField] bool showAllNearbyItemsGui;

    //-------------------------------------------
    [SerializeField]float CheckForNearstItemDelaySeconds = .2f;

    #endregion


    #region properties

    //IInteractor-------------------------------

    public List<IInteractiveItem> ItemsInRange
    {
        get { return itemsInRange; }
    }


    public GameObject Instance
    {
        get { return gameObject; }
    }

    public IInteractiveItem NearestItem
    {
        get
        {
            if (nearestItem != null)
                if (nearestItem.IsInteractionDiabled || nearestItem.IsInteractionDiabled)
                    nearestItem = null;
            return nearestItem;

        }
        set
        {
            if(!showAllNearbyItemsGui)
                if (nearestItem != value)
            {
                if (nearestItem != null)
                    nearestItem.HideGUI();

                if (value != null)
                    value.ShowGUI();
            }

            nearestItem = value;


        }
    }

    //-------------------------------------------

    #endregion


    #region methods

    //unityEvents--------------------------------

    void Awake()
    {

        StartCoroutine(FindNearestItemCoroutine());

    }

    void Update()
    {
       if(Input.GetKeyDown(InputsSettings.MainInteractionKey) && NearestItem != null)
            NearestItem.MainInteraction();

       else if (Input.GetKeyDown(InputsSettings.SecondaryInteractionKey) && NearestItem != null)
            NearestItem.SeconderyInteraction();

        OnDebugMode();
    }




    //IInteractor-------------------------------
    public void IteractWithItem(bool isMainInteraction)
    {
        if (isMainInteraction)
            NearestItem.MainInteraction();
        else
            NearestItem.SeconderyInteraction();
    }
    public void AddItem([NotNull] IInteractiveItem item)
    {

        if (!itemsInRange.Contains(item))
        {
            itemsInRange.Add(item);

            if (showAllNearbyItemsGui)
                item.ShowGUI();
        }
    }
    public void RemoveItem([NotNull] IInteractiveItem item)
    {

        if (itemsInRange.Contains(item))
        {
            if (showAllNearbyItemsGui)
                item.HideGUI();

            itemsInRange.Remove(item); ;
        }
    }

    //-------------------------------------------

    IInteractiveItem FindNearestItem()
    {
        var temp = ItemsInRange.Where(x => !x.IsInteractionDiabled).ToList();

        if (temp.Count == 0)
        {
            NearestItem = null;
            return null;
        }

        if (temp.Count == 1)
        {
            return temp[0];
        }


        var closestItemDist = (temp[0].Instance.transform.position - transform.position).sqrMagnitude;
        var closestItem = temp[0];

        for (int i = 1; i < temp.Count; i++)
        {
            var tempDist = (temp[i].Instance.transform.position - transform.position).sqrMagnitude;
            if (tempDist <= closestItemDist)
            {
                closestItem = temp[i];
                closestItemDist = tempDist;
            }
        }

        return closestItem;

    }
    IEnumerator FindNearestItemCoroutine()
    {
        while (true)
        {
            ItemsInRange.RemoveAll(x => x == null);
            ItemsInRange.RemoveAll(x => x.IsDestryed);

            NearestItem = FindNearestItem();

            yield return new WaitForSeconds(CheckForNearstItemDelaySeconds);
        }
    }

    //-------------------------------------------

    #endregion


    #region debug

    //-------------------------------------------

#if DEBUG
    [Space]
    [Space]
    [Header("debug")]

    [ReadOnly] [SerializeField] int DEBUG_ItemsInRangeCount;
    [ReadOnly] [SerializeField] GameObject DEBUG_NearestGameObjectItem;

    #endif


    void OnDebugMode()
    {
#if DEBUG

        DEBUG_ItemsInRangeCount = ItemsInRange.Count;



        if (NearestItem == null || NearestItem.IsDestryed)
            DEBUG_NearestGameObjectItem = null;
        else 
            DEBUG_NearestGameObjectItem = NearestItem.Instance;

#endif
    }

    //-------------------------------------------

    #endregion

}
