﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveDoor : InteractiveItem
{
    
    [SerializeField] private bool _interactOnlyOnce;
    bool interactedOnce;


    [Space,Space,Header("door settings")]
    [SerializeField] Transform meshToRotate;
    [SerializeField] bool isDoorOpen;
    [SerializeField] bool isDoorLocked;
    [SerializeField] float openDoorAngle;
    [SerializeField] float rotationSpeed;
    [SerializeField] float closeDoorThreshold = 5f;

    bool playerInRange;
    float rotationDirection;
    bool OpenDoor;
    bool closeDoor;


    [Space, Space, Header("notes")]
    [SerializeField] NoteMessage doorLockedNote;
    [SerializeField] NoteMessage doorOpenedNote;


    [Space, Space, Header("door locked Audio")]
    [SerializeField] bool playDoorCloseAudio = true;
    [SerializeField] bool playDoorOpenAudio = true;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip closedDoorAudioClip;
    [SerializeField] AudioClip openDoorAudioClip;


    public bool InteractOnlyOnce
    {
        get { return _interactOnlyOnce; }
        set { _interactOnlyOnce = value; }
    }



    protected override void Awake()
    {
        base.Awake();

        if (isDoorOpen)
        {
            IsDoorLocked = false;
            meshToRotate.eulerAngles = new Vector3(meshToRotate.eulerAngles.x, openDoorAngle, meshToRotate.eulerAngles.z);
        }


    }
    protected override void Update()
    {
        base.Update();
        if (isDestryed) return;


        OpenCloseDoor();

    }


 /*   protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if (other.tag == ItemInteratorTag)
        {
            playerInRange = true;

            if (!OpenDoor && !closeDoor)
                DisableInteraction(false);
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);


        if (other.tag == ItemInteratorTag)
        {
            playerInRange = false;

            if (!OpenDoor && !closeDoor)
                DisableInteraction(true);
        }
    }
*/


    private void OpenCloseDoor()
    {

        rotationDirection = OpenDoor ? 1 : (closeDoor ? -1 : 0);

        if (OpenDoor)
        {
            meshToRotate.Rotate(Vector3.up, rotationDirection*rotationSpeed*Time.deltaTime);
            if (meshToRotate.eulerAngles.y > openDoorAngle)
            {
                meshToRotate.eulerAngles = new Vector3(meshToRotate.eulerAngles.x, openDoorAngle, meshToRotate.eulerAngles.z);
                OpenDoor = false;

                if (playerInRange)
                    DisableInteraction(false);
            }
        }
        if (closeDoor)
        {
            meshToRotate.Rotate(Vector3.up, rotationDirection*rotationSpeed*Time.deltaTime);
            if (Mathf.Abs(meshToRotate.eulerAngles.y) <= closeDoorThreshold)
            {
                meshToRotate.eulerAngles = new Vector3(meshToRotate.eulerAngles.x, 0, meshToRotate.eulerAngles.z);
                closeDoor = false;

                if (playerInRange)
                    DisableInteraction(false);
            }
        }
    }
    public override void MainInteraction()
    {
        base.MainInteraction();
        if (IsInteractionDiabled) return;


        if (IsDoorLocked)
        {
            //play Audio
            if (playDoorCloseAudio)
                PlaySound(closedDoorAudioClip);

            //show note
            doorLockedNote.ShowNote();

            if (InteractOnlyOnce)
                DisableInteraction(false);

            return;
        }


        if (InteractOnlyOnce && interactedOnce == false)
        {
            interactedOnce = true;
            doorOpenedNote.ShowNote();

            if(playDoorOpenAudio)
                PlaySound(openDoorAudioClip);
        }
        else if (InteractOnlyOnce && interactedOnce)
        {
            DisableInteraction(true);
            return;
        }


        DisableInteraction(true);


        if (isDoorOpen)
        {
            isDoorOpen = false;
            closeDoor = true;
        }
        else
        {
            isDoorOpen = true;
            OpenDoor = true;
        }

    }


    public bool IsDoorLocked
    {
        get { return isDoorLocked; }
        private set { isDoorLocked = value; }
    }
    public void LockDoor(bool state)
    {
        IsDoorLocked = state;       
    }

    public void PlaySound(AudioClip clip)
    {
        if(clip == null)
            return;
        if(audioSource == null)
            return;
        if(audioSource.isPlaying)
            return;

        audioSource.clip = clip;
        audioSource.Play();
    }
}