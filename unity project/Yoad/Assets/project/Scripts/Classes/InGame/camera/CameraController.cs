﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private GameObject maincam;
    private GameObject MPCamera; // Multi Purpose Camera
    private GameObject player; // Player
    private GameObject pivotCamera; // pivot of the camera
    public float speed = 50f;//rotation speed
    public float highAngle = 300f; // if the angle above 300
    public float maxTopAngle = 30f; // max angle view from above
    public float maxBattomAngle = 325f; // max angle view from bottom

    public CamScannerManager centerScanner;
    public CamScannerManager backScanner;
    private enum ScannerResult
    {
        wallInRange, wallNotInRange, behindWall, wallOnSide
    }
    ScannerResult scannerResult = new ScannerResult();

    //----------------Scanner Check Timer---------------
    private float scannerCheckTime;
    private float scannerDelayBetweenChecks = 0.3f;
    //--------------------------------------------------

    

    private float RSHorizontal;
    private float RSVertical;


    private float minJoystickPressure = 0.3f;

    // Use this for initialization
    void Start () {
        // set all the game objects
        MPCamera = gameObject;
        maincam = GameObject.FindGameObjectWithTag("MainCamera");
        player = GameObject.FindGameObjectWithTag("Player");
        pivotCamera = transform.GetChild(0).gameObject;
        scannerCheckTime = Time.time;

    }
	
	// Update is called once per frame
	void Update () {
        Debugging();


        if (InputsSettings.IsUsingJoystick)
            JoyStickController();
        else
            KeyBoardController();

        RSHorizontal = -Input.GetAxis(InputsSettings.Camera_Axis_horizontal);
        RSVertical = -Input.GetAxis(InputsSettings.Camera_Axis_vertical);

        attemptToCheckScanners();
                
    }

    void LateUpdate()
    {
        transform.position = player.transform.position;
        DistOFMainCam();


    }

    private void DistOFMainCam()
    {
        int regularDistOfMainCam = 5;
        int minDistIfManCam = 2;
        int speed = 3;

        if(scannerResult == ScannerResult.wallInRange)
        {
            if(Vector3.Distance(maincam.transform.position, gameObject.transform.position) > (float)minDistIfManCam)
                maincam.transform.localPosition += (new Vector3(0, 0, speed) * Time.deltaTime);
        }

        else if(scannerResult == ScannerResult.wallNotInRange)
        {

            if (Vector3.Distance(maincam.transform.position, gameObject.transform.position) < (float)regularDistOfMainCam)
                maincam.transform.localPosition -= (new Vector3(0, 0, speed) * Time.deltaTime);
        }

    }

    private void attemptToCheckScanners()
    {
       if(Time.time - scannerCheckTime >= scannerDelayBetweenChecks)
        {
            CheckScanners();
            scannerCheckTime = Time.time;
        }
    }

    private void CheckScanners()
    {
        if (centerScanner.report())
        {
            if (backScanner.report())
                scannerResult = ScannerResult.wallOnSide;

            else
                scannerResult = ScannerResult.wallInRange;
        }
        else
        {
            if (backScanner.report())
                scannerResult = ScannerResult.behindWall;

            else
                scannerResult = ScannerResult.wallNotInRange;
        }
        //print(scannerResult.ToString());
    }

    private void KeyBoardController()
    {
        if (Input.GetKey(KeyCode.RightArrow))// rotate camera right
            MPCamera.transform.Rotate(Vector3.down * speed * Time.deltaTime);

        else if (Input.GetKey(KeyCode.LeftArrow))// rotate camera left
            MPCamera.transform.Rotate(Vector3.up * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.UpArrow))
        {// rotate camera up
            if (pivotCamera.transform.rotation.eulerAngles.x > highAngle)
                pivotCamera.transform.Rotate(Vector3.right * speed * Time.deltaTime);

            else if (pivotCamera.transform.rotation.eulerAngles.x < maxTopAngle)
                pivotCamera.transform.Rotate(Vector3.right * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {// rotate camera down
            if (pivotCamera.transform.rotation.eulerAngles.x < highAngle)
                pivotCamera.transform.Rotate(Vector3.left * speed * Time.deltaTime);

            else if (pivotCamera.transform.rotation.eulerAngles.x > maxBattomAngle)
                pivotCamera.transform.Rotate(Vector3.left * speed * Time.deltaTime);

        }
    }

    private void JoyStickController()
    {
        if ((RSHorizontal > minJoystickPressure) || (RSHorizontal < -minJoystickPressure))
        {
            MPCamera.transform.Rotate(Vector3.down * RSHorizontal * speed * Time.deltaTime);
        }

        if ((RSVertical > minJoystickPressure) || (RSVertical < -minJoystickPressure))
        {

            if (RSVertical < -minJoystickPressure)
            {
                if (pivotCamera.transform.rotation.eulerAngles.x > highAngle)
                {
                    print("move up");
                    pivotCamera.transform.Rotate(Vector3.left * RSVertical * speed * Time.deltaTime);
                }
                else if (pivotCamera.transform.rotation.eulerAngles.x < maxTopAngle)
                {
                    print("move up");
                    pivotCamera.transform.Rotate(Vector3.left * RSVertical * speed * Time.deltaTime);
                }
            }


            else if (RSVertical > minJoystickPressure)
            {
                if (pivotCamera.transform.rotation.eulerAngles.x < highAngle)
                {
                   // print("move down");
                    pivotCamera.transform.Rotate(Vector3.left * minJoystickPressure * speed * Time.deltaTime);
                }
                else if (pivotCamera.transform.rotation.eulerAngles.x > maxBattomAngle)
                {
                  //  print("move down");
                    pivotCamera.transform.Rotate(Vector3.left * minJoystickPressure * speed * Time.deltaTime);
                }
            }

            else if (RSVertical < -minJoystickPressure)
            {
                if (pivotCamera.transform.rotation.eulerAngles.x > highAngle)
                    pivotCamera.transform.Rotate(Vector3.left * RSVertical * speed * Time.deltaTime);

                else if (pivotCamera.transform.rotation.eulerAngles.x > maxBattomAngle)
                    pivotCamera.transform.Rotate(Vector3.left * RSVertical * speed * Time.deltaTime);
            }
        }
    }

    private void Debugging()
    {
        if(MPCamera == null)
        {
            print("ERROR cannot find MPCamera");
            MPCamera = gameObject;
        }

        if(player == null)
        {
            print("ERROR cannot find player");
            player = GameObject.FindGameObjectWithTag("Player");
        }

        if(pivotCamera == null)
        {
            print("ERROR cannot find pivotCamera");
            pivotCamera = transform.GetChild(0).gameObject;
        }
    }
}
