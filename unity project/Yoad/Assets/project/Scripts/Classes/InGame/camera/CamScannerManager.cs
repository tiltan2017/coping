﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScannerManager : MonoBehaviour
{
    private bool wallInRange;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool report()
    {
        return wallInRange;
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Structure")
            wallInRange = true;
    }
    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Structure")
            wallInRange = true;
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Structure")
            wallInRange = false;
    }
}

