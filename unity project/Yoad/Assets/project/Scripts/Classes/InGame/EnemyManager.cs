﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(NavMeshAgent))]
//[RequireComponent(typeof(SphereCollider))]
public class EnemyManager : MonoBehaviour, IEnemy 
{
    //IlivingThing-------------------------------
    [Header("HP")]
    [SerializeField] int maxHP;
    [SerializeField] bool isInvincable;
    int currentHP;

    private Action<ILivingThing> _onSpeedIncreased;
    private Action<ILivingThing> _onSpeedDecreased;
    private Action<ILivingThing> _onSpeedModifiedStart;
    private Action<ILivingThing> _onSpeedModifiedEnd;
    private Action<ILivingThing> _onStunStart;
    private Action<ILivingThing> _onStunEnd;

    //references----------------------------------
    [Space, Space, Header("references")]
    [SerializeField] MonoBehaviour movementBehaviour;
    IMovement movementInterface;

    [SerializeField] MonoBehaviour CombatMonoBehaviour;
    ICombat combatInterface;


    #region IlivingThing
    //IlivingThing-------------------------------
    public int MaxHP
    {
        get { return maxHP; }
        set { maxHP = value; }
    }
    public int CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }
    public bool IsInvincible
    {
        get { return isInvincable; }
        set { isInvincable = value; }
    }
    public GameObject Instance
    {
        get { return gameObject; }
    }
    public void RecieveDamage(int damage)
    {
        if (isInvincable)
            return;
        CurrentHP -= damage;
    }

    public Action<ILivingThing> OnSpeedIncreased
    {
        get { return _onSpeedIncreased; }
        set { _onSpeedIncreased = value; }
    }
    public Action<ILivingThing> OnSpeedDecreased
    {
        get { return _onSpeedDecreased; }
        set { _onSpeedDecreased = value; }
    }
    public Action<ILivingThing> OnSpeedModifiedStart
    {
        get { return _onSpeedModifiedStart; }
        set { _onSpeedModifiedStart = value; }
    }
    public Action<ILivingThing> OnSpeedModifiedEnd
    {
        get { return _onSpeedModifiedEnd; }
        set { _onSpeedModifiedEnd = value; }
    }
    public Action<ILivingThing> OnStunStart
    {
        get { return _onStunStart; }
        set { _onStunStart = value; }
    }
    public Action<ILivingThing> OnStunEnd
    {
        get { return _onStunEnd; }
        set { _onStunEnd = value; }
    }


    //--------------------------------------------
    #endregion

    #region ICombat
    //ICombat------------------------------------
    GameObject ICombat.Instance
    {
        get { return combatInterface.Instance; }
    }
    bool ICombat.IsEnabled
    {
        get { return combatInterface.IsEnabled; }
        set { combatInterface.IsEnabled = value; }
    }
    public bool IsCombatLocked
    {
        get { return combatInterface.IsCombatLocked; }
        set { combatInterface.IsCombatLocked = value; }
    }
    public List<string> TargetsTags
    {
        get { return combatInterface.TargetsTags; }
    }


    public void LockCombat(float delaySeconds)
    {
        combatInterface.LockCombat(delaySeconds);
    }

    public void Initialize(IMovement movement)
    {
        combatInterface.Initialize(movement);
    }

    public void Attack()
    {
        combatInterface.Attack();
    }



    Action ICombat.onAttackStart
    {
        get { return combatInterface.onAttackEnd; }
        set { combatInterface.onAttackEnd = value; }
    }
    Action ICombat.onAttackEnd
    {
        get { return combatInterface.onAttackEnd; }
        set { combatInterface.onAttackEnd = value; }
    }
    Action<ILivingThing> ICombat.onAttackHit
    {
        get { return combatInterface.onAttackHit; }
        set { combatInterface.onAttackHit = value; }
    }


    //--------------------------------------------
    #endregion

    #region IMovement
    //IMovement---------------------------------
    GameObject IMovement.Instance
    {
        get { return movementInterface.Instance; }
    }
    bool IMovement.IsEnabled
    {
        get { return movementInterface.IsEnabled; }
        set { movementInterface.IsEnabled = value; }
    }
    public bool IsMovementInputsLocked
    {
        get { return movementInterface.IsMovementInputsLocked; }
    }
    public bool IsAffectableByOutsideForces
    {
        get { return movementInterface.IsAffectableByOutsideForces; }
        set { movementInterface.IsAffectableByOutsideForces = value; }
    }
    public bool IsSpeedModified
    {
        get { return movementInterface.IsSpeedModified; }
    }

    public float RunningSpeed
    {
        get { return movementInterface.RunningSpeed; }
        set { movementInterface.RunningSpeed = value; }
    }
    public float WalkingSpeed
    {
        get { return movementInterface.WalkingSpeed; }
        set { movementInterface.WalkingSpeed = value; }
    }
    public bool IsPlayerMoving
    {
        get { return movementInterface.IsPlayerMoving; }
    }


    public void LockMovementInputs(bool state)
    {
        movementInterface.LockMovementInputs(state);
    }
    public void LockMovement(float delaySeconds)
    {
        movementInterface.LockMovement(delaySeconds);
    }

    public void ModifySpeed(int affectPercentage, float delaySeconds)
    {
        movementInterface.ModifySpeed(affectPercentage, delaySeconds);
    }
    public void ResetSpeed()
    {
        movementInterface.ResetSpeed();
    }

    public void ApplyForce(Vector3 direction, float force)
    {
        movementInterface.ApplyForce(direction, force);
    }
    public void ApplyLocalForce(Vector3 direction, float force)
    {
        movementInterface.ApplyLocalForce(direction, force);
    }


    public void MoveUpdate()
    {
        movementInterface.MoveUpdate();
    }
    public void Stun(float delaySeconds)
    {
        movementInterface.Stun(delaySeconds);
    }

    Action IMovement.OnSpeedIncreased
    {
        get { return movementInterface.OnSpeedIncreased; }
        set { movementInterface.OnSpeedIncreased = value; }
    }
    Action IMovement.OnSpeedDecreased
    {
        get { return movementInterface.OnSpeedDecreased; }
        set { movementInterface.OnSpeedDecreased = value; }
    }
    Action IMovement.OnSpeedModifiedStart
    {
        get { return movementInterface.OnSpeedModifiedStart; }
        set { movementInterface.OnSpeedModifiedStart = value; }
    }
    Action IMovement.OnSpeedModifiedEnd
    {
        get { return movementInterface.OnSpeedModifiedEnd; }
        set { movementInterface.OnSpeedModifiedEnd = value; }
    }
    Action IMovement.OnStunStart
    {
        get { return movementInterface.OnStunStart; }
        set { movementInterface.OnStunStart = value; }
    }
    Action IMovement.OnStunEnd
    {
        get { return movementInterface.OnStunEnd; }
        set { movementInterface.OnStunEnd = value; }
    }


    //-------------------------------------------
    #endregion


    void Awake()
    {
        currentHP = MaxHP;

        combatInterface = CombatMonoBehaviour.GetComponent<ICombat>();
        movementInterface = movementBehaviour.GetComponent<IMovement>();

        //initialize Combat
        (this as ICombat).Initialize(this);

        //initialize IlivingThing Events
        movementInterface.OnStunStart += () => { if (OnStunStart != null) OnStunStart.Invoke(this); };
        movementInterface.OnStunEnd += () => { if (OnStunEnd != null) OnStunEnd.Invoke(this); };
        movementInterface.OnSpeedModifiedEnd += () => { if (OnSpeedModifiedEnd != null) OnSpeedModifiedEnd.Invoke(this); };
        movementInterface.OnSpeedModifiedStart += () => { if (OnSpeedModifiedStart != null) OnSpeedModifiedStart.Invoke(this); };
        movementInterface.OnSpeedDecreased += () => { if (OnSpeedDecreased != null) OnSpeedDecreased.Invoke(this); };
        movementInterface.OnSpeedIncreased += () => { if (OnSpeedIncreased != null) OnSpeedIncreased.Invoke(this); };



    }





}



public class EnemyCombat: MonoBehaviour,ICombat
{
    [SerializeField] bool _isEnabled;
    [SerializeField] List<string> _targetsTags;
    private bool _isCombatLocked;
    private Action _onAttackStart;
    private Action _onAttackEnd;
    private Action<ILivingThing> _onAttackHit;


    Coroutine combatCoroutine;


    public GameObject Instance
    {
        get { return gameObject; }
    }
    public bool IsEnabled
    {
        get { return _isEnabled; }
        set { _isEnabled = value; }
    }
    public bool IsCombatLocked
    {
        get { return _isCombatLocked; }
        set { _isCombatLocked = value; }
    }
    public List<string> TargetsTags
    {
        get { return _targetsTags; }
    }


    public Action onAttackStart
    {
        get { return _onAttackStart; }
        set { _onAttackStart = value; }
    }
    public Action onAttackEnd
    {
        get { return _onAttackEnd; }
        set { _onAttackEnd = value; }
    }
    public Action<ILivingThing> onAttackHit
    {
        get { return _onAttackHit; }
        set { _onAttackHit = value; }
    }



    public void Attack()
    {
        throw new NotImplementedException();
    }

    public void LockCombat(float delaySeconds)
    {
        if (IsEnabled)
        {
            if (combatCoroutine != null)
            {
                StopCoroutine(combatCoroutine);
                combatCoroutine = null;
            }
            combatCoroutine = StartCoroutine(LockCombatCoroutine(delaySeconds));
        }
    }

    public void Initialize(IMovement movement)
    {
        throw new NotImplementedException();
    }

    public IEnumerator LockCombatCoroutine(float delaySeconds)
    {
        IsCombatLocked = false;
        yield return new WaitForSeconds(delaySeconds);
        IsCombatLocked = true;

        combatCoroutine = null;
    }






}