using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovementBase : MonoBehaviour, IMovement
{


    #region variables
   
    //animations variables-----------------------------------------------------

    [Header("Animations")]
    [SerializeField] Animator animator;
    [SerializeField] string AnimationFloatParamiterName = "Movement";
    [SerializeField] float runningAnimationSpeed = 1;
    [SerializeField] float walkingAnimationSpeed = 0;
    [SerializeField] float idleAnimationSpeed = -1;
    [SerializeField] string stunAnimationTriggerName = "Stunned";



    //IMovement variables-----------------------------------------------------
    [Space,Space,Header("IMovement")]
    [SerializeField] private bool _isEnabled = true;
    [SerializeField] private bool _isMovementInputsLocked;
    [SerializeField] private bool _isAffectableByOutsideForces = true;
    //--------------------------
    [SerializeField, ReadOnly] private float _currentSpeed;
    [SerializeField] private float _runningSpeed = 4f;
    [SerializeField] private float _walkingSpeed = 1f;
    //--------------------------
    private bool _isPlayerMoving;
    //--------------------------
    private Action _onSpeedIncreased;
    private Action _onSpeedDecreased;
    private Action _onSpeedModifiedStart;
    private Action _onSpeedModifiedEnd;
    private Action _onStunStart;
    private Action _onStunEnd;



    //local variables---------------------------------------------------------
    [Space, Space, Header("References")]
    [SerializeField] Transform movedObject;
    [SerializeField] NavMeshAgent navMeshAgent;
    [SerializeField] Transform rotatingMeshTransform;
    [SerializeField] Transform MPCameraHolder;

    //--------------------------
    [Space, Space, Header("Local Fields")] [SerializeField] float gravity = 20.0F;
    [SerializeField, ReadOnly] float currentAnimationSpeed;

    //--------------------------

    float initialRunnigSpeed;
    float initialWalkingSpeed;

    //--------------------------

    bool isSpeedNewlyModified; // true each time the ModifySpeed method is called
    SpeedEffect lastSpeedEffect; // the speed modification that is applied to the player

    //--------------------------

    Vector3 targetPosition; // the direction of the joystick translated into world direction
    float tempSqrtMagnetude;// a variable that stores the magnitude of the player current speed, in order to know which animation to play

    //--------------------------

    Coroutine lastLockMovementCall; // stores the last "LockMovement" Coroutine, in order to stop/reset it; 
    Coroutine lastSpeedModification; // stores the last "ModifySpeed" Coroutine, in order to stop/reset it; 
    Coroutine lastStunCoroutine; // stores the last "Stun" Coroutine, in order to stop/reset it; 



    //------------------------------------------------------------------------

    #endregion

    #region properties

    //IMovement properties----------------------------------------------------
    public GameObject Instance
    {
        get { return gameObject; }
    }

    /// <summary>
    /// a property that enables/disables the movement of the script user
    /// </summary>
    public bool IsEnabled
    {
        get { return _isEnabled; }
        set { _isEnabled = value; }
    }

    /// <summary>
    /// if true, all methods that modify/affect the movement are ignored
    /// </summary>
    public bool IsAffectableByOutsideForces
    {
        get { return _isAffectableByOutsideForces; }
        set { _isAffectableByOutsideForces = value; }
    }

    /// <summary>
    /// true, if the movement is locked
    /// </summary>
    public bool IsMovementInputsLocked
    {
        get { return _isMovementInputsLocked; }
    }

    /// <summary>
    /// returns true if the speed is modified
    /// </summary>
    public bool IsSpeedModified
    {
        get { return Math.Abs(RunningSpeed - initialRunnigSpeed) > .05f; }
    }

    /// <summary>
    /// returns or sets the running speed of the user
    /// </summary>
    public float RunningSpeed
    {
        get { return _runningSpeed; }
        set
        {
            _runningSpeed = value;

            if (value < WalkingSpeed)
                WalkingSpeed = RunningSpeed;
        }
    }

    /// <summary>
    ///returns or sets the walking speed of the user
    /// </summary>
    public float WalkingSpeed
    {
        get { return _walkingSpeed; }
        set
        {
            _walkingSpeed = value;
            if (value > RunningSpeed)
                RunningSpeed = value;
        }
    }

    /// <summary>
    /// returns true if the player is moving using inputs
    /// (returns false if the movement is locked)
    /// </summary>
    public bool IsPlayerMoving
    {
        get
        {
            if (IsMovementInputsLocked)
                return false;

            return _isPlayerMoving;
        }
    }


    //IMovement Events--------------------------------------------------------

    public Action OnSpeedIncreased
    {
        get { return _onSpeedIncreased; }
        set { _onSpeedIncreased = value; }
    }
    public Action OnSpeedDecreased
    {
        get { return _onSpeedDecreased; }
        set { _onSpeedDecreased = value; }
    }
    public Action OnSpeedModifiedStart
    {
        get { return _onSpeedModifiedStart; }
        set { _onSpeedModifiedStart = value; }
    }
    public Action OnSpeedModifiedEnd
    {
        get { return _onSpeedModifiedEnd; }
        set { _onSpeedModifiedEnd = value; }
    }
    public Action OnStunStart
    {
        get { return _onStunStart; }
        set { _onStunStart = value; }
    }
    public Action OnStunEnd
    {
        get { return _onStunEnd; }
        set { _onStunEnd = value; }
    }


    #endregion

    #region methods

    //MonoBehavior events-----------------------------------------------------
    void Start()
    {
        initialRunnigSpeed = RunningSpeed;
        initialWalkingSpeed = WalkingSpeed;

    }
    //IMovement methods-------------------------------------------------------

    /// <summary>
    /// locks the player from moving. ( ignores the movement inputs ).
    /// the method also resets the "LockMovement(float delaySeconds)" method
    /// </summary>
    /// <param name="state"> if true, the movement gets locked, otherwise it's not </param>
    public void LockMovementInputs(bool state)
    {
        if (!IsEnabled)
            return;

        //stop last "LockMovement(float delaySeconds)" if is still active
        if (lastLockMovementCall != null)
        {
            StopCoroutine(lastLockMovementCall);
            lastLockMovementCall = null;
        }

        //apply the effect
        _isMovementInputsLocked = state;
    }


    /// <summary>
    /// locked movement for a certain amount of seconds.
    /// if movement is already locked, the call is ignored
    /// </summary>
    /// <param name="delaySeconds">the effect duration</param>
    public void LockMovement(float delaySeconds)
    {
        if (!IsEnabled)
            return;

        //reset the effect, if lock Milliseconds was called last
        if (lastLockMovementCall != null)
        {
            StopCoroutine(lastLockMovementCall);
            lastLockMovementCall = null;
        }

        //if the movement is already locked, don't do anything
        else if (IsMovementInputsLocked)
            return;

        //lock olayer movement for a certiondelay
        lastLockMovementCall = StartCoroutine(LockMovementDelayCoroutine(delaySeconds));
    }

    IEnumerator LockMovementDelayCoroutine(float delaySeconds)
    {
        //if movement is not already locked
        if (!IsMovementInputsLocked)
        {
            //lock movement
            _isMovementInputsLocked = true;

           // navMeshAgent.speed = 0;
           // navMeshAgent.angularSpeed = 0;

            //wait for certain amount of seconds
            yield return new WaitForSeconds(delaySeconds);

           


            //unlock movement
            _isMovementInputsLocked = false;

            //dereference the this method/coroutine, as it has finished applying the effect
            lastLockMovementCall = null;
        }
    }

    //-----------------------------

    /// <summary>
    /// modifies (increases/decreases) the speed of the user by a given percentage, for a certain amount of seconds.
    /// </summary>
    /// <param name="affectPercentage">the modification percentage</param>
    /// <param name="delaySeconds">the effect duration</param>
    public void ModifySpeed(int affectPercentage, float delaySeconds)
    {
        if (!IsEnabled)
            return;


        if (lastSpeedModification != null)
            StopCoroutine(lastSpeedModification);

        SpeedEffect tempEffect = new SpeedEffect(affectPercentage, delaySeconds);
        lastSpeedEffect += tempEffect;
        lastSpeedModification = StartCoroutine(ModifySpeedCoroutine(delaySeconds));
    }

    IEnumerator ModifySpeedCoroutine(float delaySeconds)
    {
        
        //break if speed is not modified
        if(Math.Abs(lastSpeedEffect.AffectPercentage) < .05f)
        {
            lastSpeedModification = null;
            WalkingSpeed = initialWalkingSpeed;
            RunningSpeed = initialRunnigSpeed;
            yield break;
        }

        //invoke events
        InvokeSpeedModificationStartEvents();

        //apply speed modification
        WalkingSpeed = (lastSpeedEffect.AffectPercentage/100f)*WalkingSpeed;
        RunningSpeed = (lastSpeedEffect.AffectPercentage/100f)*RunningSpeed;

        //wait for certain amount of seconds
        yield return new WaitForSeconds(delaySeconds);

        //reset speed
        WalkingSpeed = initialWalkingSpeed;
        RunningSpeed = initialRunnigSpeed;

        //invoke events
        InvokeSpeedModificationEndEvents();

        //dereference this coroutine as ti finished it's task
        lastSpeedModification = null;
    }
    void InvokeSpeedModificationStartEvents()
    {
        if(lastSpeedEffect.IsSpeedup)
            if(OnSpeedIncreased!= null)
                OnSpeedIncreased.Invoke();

        if (lastSpeedEffect.IsSpeedDown)
            if(OnSpeedIncreased!= null)
                OnSpeedIncreased.Invoke();

        if (OnSpeedModifiedStart != null)
            OnSpeedModifiedStart.Invoke();
    }
    void InvokeSpeedModificationEndEvents()
    {
        //invoke events
        if (OnSpeedModifiedEnd != null)
            OnSpeedModifiedEnd.Invoke();
    }



    /// <summary>
    /// resets the walking and running speed of the player to their initial values
    /// </summary>
    public void ResetSpeed()
    {
        if (lastSpeedModification != null)
        {
            StopCoroutine(lastSpeedModification);
            lastSpeedModification = null;
            InvokeSpeedModificationEndEvents();
        }
        RunningSpeed = initialRunnigSpeed;
        WalkingSpeed = initialWalkingSpeed;
    }


    //-----------------------------

    /// <summary>
    /// applies force in a word space direction
    /// </summary>
    /// <param name="direction">the direction in which the force is applied</param>
    /// <param name="force">the magnitude of the force</param>
    public void ApplyForce(Vector3 direction, float force)
    {
        if (!IsAffectableByOutsideForces || !IsEnabled)
            return;
        
        direction = direction.normalized;
        navMeshAgent.velocity = direction*force;



    }


    /// <summary>
    /// applies force in a local direction relative to the mesh
    /// </summary>
    /// <param name="direction">the direction in which the force is applied</param>
    /// <param name="force">the magnitude of the force</param>
    public void ApplyLocalForce(Vector3 direction, float force)
    {
        ApplyForce(rotatingMeshTransform.TransformVector(direction), force);
    }


    //-----------------------------

    /// <summary>
    /// lock player movement and play stun animation.
    /// </summary>
    /// <param name="delaySeconds">stun duration in seconds</param>
    public void Stun(float delaySeconds)
    {
        if(lastStunCoroutine != null)
            StopCoroutine(lastStunCoroutine);

        lastStunCoroutine = StartCoroutine(StunPlayAnimationCoroutine(delaySeconds));
    }

    IEnumerator StunPlayAnimationCoroutine(float delaySeconds)
    {
        LockMovement(delaySeconds);

        if(OnStunStart!= null)
            OnStunStart.Invoke();

        animator.SetBool(stunAnimationTriggerName, true);

        yield return new WaitForSeconds(delaySeconds);

        animator.SetBool(stunAnimationTriggerName, false);

        if (OnStunEnd != null)
            OnStunEnd.Invoke();
    }

    //other methods-----------------------------------------------------------

    public void MoveUpdate()
    {
 /*       Vector2 inputsMovementAxis =
            new Vector2(Input.GetAxis(InputsSettings.Movement_Axis_horizontal),
                Input.GetAxis(InputsSettings.Movement_Axis_vertical) * ((InputsSettings.IsUsingJoystick) ? (-1) : 1));


        //if the player is on the ground
        if (characterController.isGrounded)
        {
            //covert movement inputs to world space vector
            Vector3 dir = GetMovementDirection(inputsMovementAxis);

            //multiply by the running speed
            moveDirection = dir*RunningSpeed;


            //make player mesh face the direction of the vector 
            rotatingMeshTransform.LookAt(rotatingMeshTransform.position + dir);


            //if the player is moving, limit the speed between 0 and running speed, and play the matching animation
            if (dir != Vector3.zero)
            {
                _isPlayerMoving = true;

                //get the squared length of the current speed
                tempSqrtMagnetude = moveDirection.sqrMagnitude;

                //if the current speed is lesser than the walking speed
                if (tempSqrtMagnetude < WalkingSpeed*WalkingSpeed)
                {
                    //set the current speed to the walking speed
                    moveDirection = dir.normalized*WalkingSpeed;

                    //set the animation speed
                    currentAnimationSpeed = walkingAnimationSpeed;

                }

                //if the current speed is greater than the running Speed 
                else if (tempSqrtMagnetude > RunningSpeed*RunningSpeed)
                {
                    moveDirection = dir.normalized*RunningSpeed;
                    currentAnimationSpeed = runningAnimationSpeed;

                }

                //otherwise set the animation speed based on the player's speed
                else
                    currentAnimationSpeed = walkingAnimationSpeed +
                                            tempSqrtMagnetude/(RunningSpeed*RunningSpeed)*
                                            (runningAnimationSpeed - walkingAnimationSpeed);


            }

            //if the player is not moving
            else
            {
                currentAnimationSpeed = idleAnimationSpeed;
                _isPlayerMoving = false;
            }

        }



        //add gravity
        moveDirection.y -= gravity*Time.deltaTime;


        characterController.Move(moveDirection*Time.deltaTime);

        //update the animation speed
        animator.SetFloat(AnimationFloatParamiterName, currentAnimationSpeed);

    */
    }


    /// <summary>
    /// converts movement inputs to world direction based on camera's position.
    /// 
    /// </summary>
    /// <returns>world space vector</returns>
    Vector3 GetMovementDirection(Vector2 inputsMovementAxis)
    {
        if(IsMovementInputsLocked || !IsEnabled)
            return Vector3.zero;

        Vector3 dir;

        //create a vector from inputs
        dir = new Vector3(inputsMovementAxis.x,0, inputsMovementAxis.y);

        //eliminate input noises
        if (dir.sqrMagnitude < InputsSettings.Joystick_Sticks_Noise)
            dir = Vector3.zero;

        //transform the input direction to world space direction
        if (MPCameraHolder != null)
            dir = MPCameraHolder.TransformDirection(dir);

        //return the direction
        return dir;
    }

    //------------------------------------------------------------------------

    #endregion

    #region inner classes

    /// <summary>
    /// an inner class that is used to simplify player speed modification calculations
    /// </summary>
    struct SpeedEffect
    {
        static float floatEqualsthreshold = .05f;

        //fields
        private int affectPercentage;
        private float delayMilliseconds;
        private float startTimeSample;


        //constructor
        public SpeedEffect(int affectPercentage, float delayMilliseconds)
        {
            this.affectPercentage = affectPercentage;
            this.delayMilliseconds = delayMilliseconds;
            this.startTimeSample = Time.time;
        }


        //properties
        public float StartTimeSample
        {
            get { return startTimeSample; }
            set { startTimeSample = value; }
        }

        public int AffectPercentage
        {
            get { return affectPercentage; }
            set { affectPercentage = value; }
        }

        public float DelayMilliseconds
        {
            get { return delayMilliseconds; }
            set { delayMilliseconds = value; }
        }


        public bool IsSpeedup
        {
            get { return affectPercentage > 100; }
        }

        public bool IsSpeedDown
        {
            get { return affectPercentage < 100 && affectPercentage > .05; }
        }

        public bool IsNullEffect
        {
            get { return Math.Abs(affectPercentage) < .05 || Math.Abs(affectPercentage - 100) < .05; }
        }

        public bool IsEffectEnded
        {
            get
            {
                if (!IsNullEffect)
                    return (Time.time - startTimeSample) > (delayMilliseconds/1000f);

                return true;
            }
        }


        public override bool Equals(object obj)
        {
            if (!(obj is SpeedEffect))
                return false;

            var other = (SpeedEffect) obj;

            if (Math.Abs(affectPercentage - other.affectPercentage) > floatEqualsthreshold)
                return false;

            return Math.Abs(delayMilliseconds - other.delayMilliseconds) < floatEqualsthreshold;
        }

        public static bool operator ==(SpeedEffect obj1, SpeedEffect obj2)
        {
            return obj1.Equals(obj2);
        }

        public static bool operator !=(SpeedEffect obj1, SpeedEffect obj2)
        {
            return !obj1.Equals(obj2);
        }

        public static SpeedEffect operator +(SpeedEffect obj1, SpeedEffect obj2)
        {
            //return null effect if given two null effects
            if (obj1.IsNullEffect && obj2.IsNullEffect)
                return obj1;

            //if the first effect has ended and the second has not
            if (obj1.IsEffectEnded && !obj2.IsNullEffect)
                return obj2;

            //if the second effect has ended and the first has not
            if (obj2.IsEffectEnded && !obj1.IsNullEffect)
                return obj1;

            //if both effects has ended, return null
            if (obj1.IsEffectEnded && obj2.IsEffectEnded)
                return new SpeedEffect();


            //find the effect that started first
            SpeedEffect firstEffect = obj1.StartTimeSample < obj2.startTimeSample ? obj1 : obj2;

            //find the effect that started second
            SpeedEffect secondEffect = obj1.StartTimeSample > obj2.startTimeSample ? obj1 : obj2;

            //if first effect is a speed down and the second effect is a speed up
            //cancel the speed down effect
            if (firstEffect.IsSpeedDown && secondEffect.IsSpeedup)
                return secondEffect;

            if (firstEffect.IsSpeedup && secondEffect.IsSpeedDown)
                return firstEffect;

            if (firstEffect.IsSpeedup && secondEffect.IsSpeedup)
                return secondEffect;

            // if (firstEffect.IsSpeedDown && secondEffect.IsSpeedDown)
            return secondEffect;

        }

    }

    #endregion

}