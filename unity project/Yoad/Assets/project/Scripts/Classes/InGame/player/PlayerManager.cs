using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;



[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController))]

public class PlayerManager : MonoBehaviour, IPlayer
{



    #region fields

    //IlivingThing-------------------------------
    [Header("HP")]
    [SerializeField] int maxHP;
    [SerializeField] bool isInvincable;
    int currentHP;

    private Action<ILivingThing> _onSpeedIncreased;
    private Action<ILivingThing> _onSpeedDecreased;
    private Action<ILivingThing> _onSpeedModifiedStart;
    private Action<ILivingThing> _onSpeedModifiedEnd;
    private Action<ILivingThing> _onStunStart;
    private Action<ILivingThing> _onStunEnd;

    //references----------------------------------
    [Space, Space, Header("references")]
    [SerializeField] PlayerMovement movementManager;
    [SerializeField] MonoBehaviour playerCombatMonoBehaviour;
    ICombat playerCombat;


    #endregion

    #region IlivingThing
    //IlivingThing-------------------------------
    public int MaxHP
    {
        get { return maxHP; }
        set { maxHP = value; }
    }
    public int CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }
    public bool IsInvincible
    {
        get { return isInvincable; }
        set { isInvincable = value; }
    }
    public GameObject Instance 
    {
        get { return gameObject; }
    }
    public void RecieveDamage(int damage)
    {
        if(isInvincable)
            return;
        CurrentHP -= damage;
    }

    public Action<ILivingThing> OnSpeedIncreased
    {
        get { return _onSpeedIncreased; }
        set { _onSpeedIncreased = value; }
    }
    public Action<ILivingThing> OnSpeedDecreased
    {
        get { return _onSpeedDecreased; }
        set { _onSpeedDecreased = value; }
    }
    public Action<ILivingThing> OnSpeedModifiedStart
    {
        get { return _onSpeedModifiedStart; }
        set { _onSpeedModifiedStart = value; }
    }
    public Action<ILivingThing> OnSpeedModifiedEnd
    {
        get { return _onSpeedModifiedEnd; }
        set { _onSpeedModifiedEnd = value; }
    }
    public Action<ILivingThing> OnStunStart
    {
        get { return _onStunStart; }
        set { _onStunStart = value; }
    }
    public Action<ILivingThing> OnStunEnd
    {
        get { return _onStunEnd; }
        set { _onStunEnd = value; }
    }


    //--------------------------------------------
    #endregion

    #region ICombat
    //ICombat------------------------------------
    GameObject ICombat.Instance
    {
        get { return playerCombat.Instance; }
    }
    bool ICombat.IsEnabled
    {
        get { return playerCombat.IsEnabled; }
        set { playerCombat.IsEnabled = value; }
    }
    public bool IsCombatLocked
    {
        get { return playerCombat.IsCombatLocked; }
        set { playerCombat.IsCombatLocked = value; }
    }
    public List<string> TargetsTags
    {
        get { return playerCombat.TargetsTags; }
    }


    public void LockCombat(float delaySeconds)
    {
        playerCombat.LockCombat(delaySeconds);
    }

    public void Initialize(IMovement movement)
    {
        playerCombat.Initialize(movement);
    }

    public void Attack()
    {
        playerCombat.Attack();
    }



     Action ICombat.onAttackStart
    {
        get { return playerCombat.onAttackEnd; }
        set { playerCombat.onAttackEnd = value; }
    }

     Action ICombat.onAttackEnd
    {
        get { return playerCombat.onAttackEnd; }
        set { playerCombat.onAttackEnd = value; }
    }
     Action<ILivingThing> ICombat.onAttackHit
    {
        get { return playerCombat.onAttackHit; }
        set { playerCombat.onAttackHit = value; }
    }


    //--------------------------------------------
    #endregion

    #region IMovement
    //IMovement---------------------------------
    GameObject IMovement.Instance
    {
        get { return movementManager.Instance; }
    }
    bool IMovement.IsEnabled
    {
        get { return movementManager.IsEnabled; }
        set { movementManager.IsEnabled = value; }
    }
    public bool IsMovementInputsLocked
    {
        get { return movementManager.IsMovementInputsLocked; }
    }
    public bool IsAffectableByOutsideForces
    {
        get { return movementManager.IsAffectableByOutsideForces; }
        set { movementManager.IsAffectableByOutsideForces = value; }
    }
    public bool IsSpeedModified
    {
        get { return movementManager.IsSpeedModified; }
    }

    public float RunningSpeed
    {
        get { return movementManager.RunningSpeed; }
        set { movementManager.RunningSpeed = value; }
    }
    public float WalkingSpeed
    {
        get { return movementManager.WalkingSpeed; }
        set { movementManager.WalkingSpeed = value; }
    }
    public bool IsPlayerMoving
    {
        get { return movementManager.IsPlayerMoving; }
    }


    public void LockMovementInputs(bool state)
    {
        movementManager.LockMovementInputs(state);
    }
    public void LockMovement(float delaySeconds)
    {
        movementManager.LockMovement(delaySeconds);
    }

    public void ModifySpeed(int affectPercentage, float delaySeconds)
    {
        movementManager.ModifySpeed(affectPercentage, delaySeconds);
    }
    public void ResetSpeed()
    {
        movementManager.ResetSpeed();
    }

    public void ApplyForce(Vector3 direction, float force)
    {
        movementManager.ApplyForce(direction,force);
    }
    public void ApplyLocalForce(Vector3 direction, float force)
    {
        movementManager.ApplyLocalForce(direction,force);
    }


    public void MoveUpdate()
    {
        movementManager.MoveUpdate();
    }
    public void Stun(float delaySeconds)
    {
        movementManager.Stun(delaySeconds);
    }

    Action IMovement.OnSpeedIncreased
    {
        get { return movementManager.OnSpeedIncreased; }
        set { movementManager.OnSpeedIncreased = value; }
    }
    Action IMovement.OnSpeedDecreased
    {
        get { return movementManager.OnSpeedDecreased; }
        set { movementManager.OnSpeedDecreased = value; }
    }
    Action IMovement.OnSpeedModifiedStart
    {
        get { return movementManager.OnSpeedModifiedStart; }
        set { movementManager.OnSpeedModifiedStart = value; }
    }
    Action IMovement.OnSpeedModifiedEnd
    {
        get { return movementManager.OnSpeedModifiedEnd; }
        set { movementManager.OnSpeedModifiedEnd = value; }
    }
    Action IMovement.OnStunStart
    {
        get { return movementManager.OnStunStart; }
        set { movementManager.OnStunStart = value; }
    }
    Action IMovement.OnStunEnd
    {
        get { return movementManager.OnStunEnd; }
        set { movementManager.OnStunEnd = value; }
    }


    //-------------------------------------------
    #endregion


    //unityEvents--------------------------------

    void Awake()
    {
        currentHP = MaxHP;

        playerCombat = playerCombatMonoBehaviour.GetComponent<ICombat>();

        //initialize Combat
        Initialize(this);

        //initialize IlivingThing Events
        movementManager.OnStunStart += () => { if(OnStunStart != null) OnStunStart.Invoke(this); };
        movementManager.OnStunEnd += () => { if(OnStunEnd != null) OnStunEnd.Invoke(this); };
        movementManager.OnSpeedModifiedEnd += () => { if(OnSpeedModifiedEnd != null) OnSpeedModifiedEnd.Invoke(this); };
        movementManager.OnSpeedModifiedStart += () => { if(OnSpeedModifiedStart != null) OnSpeedModifiedStart.Invoke(this); };
        movementManager.OnSpeedDecreased += () => { if(OnSpeedDecreased != null) OnSpeedDecreased.Invoke(this); };
        movementManager.OnSpeedIncreased += () => { if(OnSpeedIncreased != null) OnSpeedIncreased.Invoke(this); };



    }


    void Update()
    {
/*        Vector2 movementDirection =
            new Vector2(Input.GetAxis(InputsSettings.Movement_Axis_horizontal),
                Input.GetAxis(InputsSettings.Movement_Axis_vertical) * ((InputsSettings.IsUsingJoystick) ? (-1) : 1));

        MoveUpdate(movementDirection);*/


        if (Input.GetKeyDown(InputsSettings.AttakKey))
            Attack();
        
    }



    //-------------------------------------------







}