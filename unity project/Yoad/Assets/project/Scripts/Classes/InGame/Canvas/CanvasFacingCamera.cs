﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFacingCamera : MonoBehaviour
{

    Transform pos;
	// Use this for initialization
	void Start ()
	{
	    pos = transform;
	}
	
	// Update is called once per frame
	void Update () {
		
        pos.LookAt(Camera.main.transform.position);
	}
}
