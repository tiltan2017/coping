﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using JetBrains.Annotations;
using UnityEngine;

public class MultiCollitionWarriorCombat : MonoBehaviour,ICombat
{


    #region fields 

    //references---------------------------------
    [Header("References")]
    [SerializeField] Animator animator;
    [SerializeField] AreaAffector areaAffector;
    

    //ICombat------------------------------------
    [Space,Space,Header("ICombat")]
    [SerializeField] bool _isEnabled = true;
    [SerializeField] List<string> _enemyTags;

    private bool _isCombatLocked;
    private Action _onAttackStart;
    private Action _onAttackEnd;
    private Action<ILivingThing> _onAttackHit;
    
    //-------------------------------------------
    [Space, Space, Header("Combat")]
    [SerializeField] List<MultiCollisionMeleeAttack> attacks;

    int currentAttackIndex;
    float currentDuration = -100;
    bool inCombo;
    Coroutine combatCoroutine;
    IMovement userMovement = null;
    float deltaTime = -5;

    //-------------------------------------------

    #endregion

    #region properties

    //ICombat------------------------------------
    public bool IsCombatLocked
    {
        get { return _isCombatLocked; }
        set { _isCombatLocked = value; }
    }

    public List<string> TargetsTags
    {
        get { return _enemyTags ?? (_enemyTags = new List<string>()); }
        private set { _enemyTags = value; }
    }

    public GameObject Instance
    {
        get { return gameObject; }
    }

    public bool IsEnabled
    {
        get
        {
            return _isEnabled;
        }
        set
        {
            _isEnabled = value;
        }
    }
    //-------------------------------------------


    public Action onAttackStart
    {
        get { return _onAttackStart; }
        set { _onAttackStart = value; }
    }

    public Action onAttackEnd
    {
        get { return _onAttackEnd; }
        set { _onAttackEnd = value; }
    }

    public Action<ILivingThing> onAttackHit
    {
        get { return _onAttackHit; }
        set { _onAttackHit = value; }

    }

    //-------------------------------------------
    public List<MultiCollisionMeleeAttack> Attacks
    {
        get
        {
            if (attacks == null)
                attacks = new List<MultiCollisionMeleeAttack>();
            return attacks;
        }
        private set { attacks = value; }
    }
    MultiCollisionMeleeAttack currentAttack { get { return Attacks[currentAttackIndex]; } }
    //-------------------------------------------
    #endregion

    #region methods

    //unityEvents--------------------------------

    void Awake()
    {
        if(areaAffector)
            areaAffector.Inintialize(TargetsTags);

        //playerMovement = playerManager;
    }

    void Update()
    {

        if (!IsEnabled || !areaAffector)
        {

            if (!inCombo)
                ExitCombatMode();
            return;
        }
        if (Attacks.Count == 0) return;


         deltaTime = Time.time - currentDuration;


        if (inCombo)//if in combat mode or player moved, causing the combo to break
        {
            if (deltaTime > currentAttack.resetComboAfterDelay || userMovement.IsPlayerMoving)
                ExitCombatMode();
        }


        //  print(transform.forward);
    }


    //ICombat------------------------------------

    public void LockCombat(float delaySeconds)
    {
        if (IsEnabled)
        {
            if (combatCoroutine != null)
            {
                StopCoroutine(combatCoroutine);
                combatCoroutine = null;
            }
            combatCoroutine = StartCoroutine(LockCombatCoroutine(delaySeconds));
        }
    }

    public IEnumerator LockCombatCoroutine(float delaySeconds)
    {
        IsCombatLocked = false;
        yield return new WaitForSeconds(delaySeconds);
        IsCombatLocked = true;

        combatCoroutine = null;
    }

    public void Attack()
    {
        if (!IsEnabled || !areaAffector)
            return;

            if (inCombo)
            {
                //if player continued the combo
                if (deltaTime > currentAttack.delayBeforeNextAttack)
                    Hit_PrepareNextHit();

            }
            else
            {
                //if the player started the combo
                BeginCombo();
                Hit_PrepareNextHit();
            }
        

    }

    public void Initialize(IMovement movement)
    {
        userMovement = movement;
    }
    //-------------------------------------------


    private void BeginCombo() 
    {
        inCombo = true;

        if (onAttackStart!= null)
            onAttackStart.Invoke();

        userMovement.LockMovement(currentAttack.delayBeforeNextAttack);
        //lock player movement

    }
    private void Hit_PrepareNextHit()
    {
        PlayAttackAnimation();

        StartCoroutine(OnHitEnemies(currentAttack));
        StartCoroutine(AffectUser(currentAttack));


        /*
                if (onAttackComboContinue != null)
                    onAttackComboContinue.Invoke(currentAttack);*/

        SetupNextAttack();
        
    }


    void PlayAttackAnimation()
    {
        animator.SetTrigger(attacks[currentAttackIndex].animatationTransactionName);
    }
    private void SetupNextAttack()
    {

        currentDuration = Time.time;
        currentAttackIndex++;

        if (currentAttackIndex == attacks.Count)
            currentAttackIndex = 0;

    }


    IEnumerator OnHitEnemies(MultiCollisionMeleeAttack attack)
    {
        yield return new WaitForSeconds(attack.hitEnemyAfterDelay);

        areaAffector.Affect((target) =>
        {
            if (onAttackHit != null)
                onAttackHit.Invoke(target);
            attack.Affect(target);
        });

    }
    IEnumerator AffectUser(MultiCollisionMeleeAttack attack)
    {
        yield return new WaitForSeconds(attack.pushPlayerForwardAfterDelay);

        userMovement.ApplyLocalForce(Vector3.forward + Vector3.up * .1f, attack.pushPlayerForwardForce);
    }



    void ExitCombatMode()
    {

        foreach (var atk in attacks)       
            animator.ResetTrigger(atk.animatationTransactionName);
                 
        //play animation
        animator.SetTrigger("ResetAttacks");

        inCombo = false;  
        currentAttackIndex = 0;

        if(onAttackEnd != null)
            onAttackEnd.Invoke();
       // playerMovement.LockMovementInputs(false);
    }

    //-------------------------------------------




    /*
     * attack
     * check for movement
     * if there's no movement, player hasn't preformed a combo, return to idle
     * if player continued a combo, setup next attack
     *  
     */
    //-------------------------------------------


    #endregion




}