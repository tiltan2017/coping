﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaAffector:MonoBehaviour
{

    //fields
    [SerializeField] List<string> _targetsTags;
    List<ILivingThing> _targetsInRange;



    //properties
    public List<ILivingThing> TargetsInRange
    {
        get
        {             
            return _targetsInRange?? (_targetsInRange = new List<ILivingThing>());
        }
        private set { _targetsInRange = value; }
    }
    public List<string> TargetsTags
    {
        get { return _targetsTags ?? (_targetsTags = new List<string>()); }
        private set { _targetsTags = value; }
    }



    //unity events
    private void OnTriggerEnter(Collider other)
    {
        if (TargetsTags.Contains(other.tag))
        {
            var enemy = other.GetComponent<ILivingThing>();
            if (!TargetsInRange.Contains(enemy))
                TargetsInRange.Add(enemy);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (TargetsTags.Contains(other.tag))
        {
            var enemy = other.GetComponent<ILivingThing>();
            if (TargetsInRange.Contains(enemy))
                TargetsInRange.Remove(enemy);
        }
    }


    //methods
    public void Inintialize(List<string> targetsTags)
    {
        TargetsTags = targetsTags;
        TargetsInRange.Clear();
    }
    public void Affect(Action<ILivingThing> affect)
    {
        TargetsInRange.ForEach(affect.Invoke);
    }
    public void Affect(Action<ILivingThing> affect,float delaySeconds)
    {
        StartCoroutine(AffectDelayCoroutine(affect, delaySeconds));
    }
    private IEnumerator AffectDelayCoroutine(Action<ILivingThing> affect, float delaySeconds)
    {
        yield return new WaitForSeconds(delaySeconds);
        Affect(affect);
    }



    #region debug

    //-------------------------------------------

#if DEBUG

    [Space, Space, Header("debug")]
    [SerializeField, ReadOnly] int ItemsInRangeCount;

    void Update()
    {
        ItemsInRangeCount = TargetsInRange.Count;
    }

#endif

    //-------------------------------------------

    #endregion
}