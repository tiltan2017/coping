﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExternalCollider : MonoBehaviour {


    public Action<Collision> onCollitionEnter;
    public Action<Collision> onCollisionExit;
    public Action<Collision> onCollitionStay;

    public Action<Collider> onTriggerEnter;
    public Action<Collider> onTriggerExit;
    public Action<Collider> onTriggerStay;


    public Action<Collision2D> onCollisionEnter2D;
    public Action<Collision2D> onCollisionExit2D;
    public Action<Collision2D> onCollisionStay2D;

    public Action<GameObject> onParticleCollision;
    public Action onParticleTrigger;



    private void OnCollisionEnter(Collision collision)
    {
        if(onCollitionEnter != null)
            onCollitionEnter.Invoke(collision);
    }
    private void OnCollisionExit(Collision collision)
    {
        if (onCollisionExit != null)
            onCollisionExit.Invoke(collision);
    }
    private void OnCollisionStay(Collision collision)
    {
        if (onCollitionStay != null)
            onCollitionStay.Invoke(collision);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (onTriggerEnter != null)
            onTriggerEnter.Invoke(other);
    }
    private void OnTriggerExit(Collider other)
    {
        if (onTriggerExit != null)
            onTriggerExit.Invoke(other);

    }
    private void OnTriggerStay(Collider other)
    {
        if (onTriggerStay != null)
            onTriggerStay.Invoke(other);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (onCollisionEnter2D != null)
            onCollisionEnter2D.Invoke(collision);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (onCollisionExit2D != null)
            onCollisionExit2D.Invoke(collision);
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (onCollisionStay2D != null)
            onCollisionStay2D.Invoke(collision);
    }

    private void OnParticleCollision(GameObject other)
    {
        if (onParticleCollision != null)
            onParticleCollision.Invoke(other);
    }
    private void OnParticleTrigger()
    {
        if (onParticleTrigger != null)
            onParticleTrigger.Invoke();
    }
}
