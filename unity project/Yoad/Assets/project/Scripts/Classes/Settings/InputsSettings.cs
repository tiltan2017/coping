﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputsSettings : MonoBehaviour
{
    //-----------------------------------------------------------------------------------------
    public static InputsSettings Instance { get; private set; }
    //-----------------------------------------------------------------------------------------
    public static bool IsUsingJoystick { get { return Instance.isUsingJoystick; } set { Instance.isUsingJoystick = value;} }
    public static float Joystick_Sticks_Noise { get { return Instance.joystickInputNoise; } }
    //-----------------------------------------------------------------------------------------
    public static string Movement_Axis_horizontal { get { return IsUsingJoystick? Instance.axislsHorizontal : Instance.axisHorizontal; } }
    public static string Movement_Axis_vertical { get { return IsUsingJoystick ? Instance.axisLsVertical : Instance.axisVertical; } }
    //-----------------------------------------------------------------------------------------
    public static string Camera_Axis_horizontal { get { return IsUsingJoystick ? Instance.axisRsHorizontal: Instance.axisCameraHorizontal; } }
    public static string Camera_Axis_vertical { get { return IsUsingJoystick ? Instance.axisRsVertical: Instance.axisCameraVertical; } }
    //-----------------------------------------------------------------------------------------
    public static KeyCode AttakKey { get { return IsUsingJoystick ? Instance.keyJoystickAttack : Instance.keyAttack; } }
    public static KeyCode MainInteractionKey { get { return IsUsingJoystick ? Instance.keyJoystickMainInteraction : Instance.keyMainInteraction; } }
    public static KeyCode SecondaryInteractionKey { get { return IsUsingJoystick ? Instance.keyJoystickSecondaryInteraction : Instance.keySecondaryInteraction; } }
    //-----------------------------------------------------------------------------------------
    public static KeyCode Skip { get { return IsUsingJoystick ? Instance.keyJoystickSkip : Instance.KeySkip; } }
    public static KeyCode ProgressDialog { get { return IsUsingJoystick ? Instance.keyJoystickProgressDialog : Instance.KeyProgressDialog; } }

    //-----------------------------------------------------------------------------------------

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }


    [SerializeField] bool isUsingJoystick;



    [Space, Space, Header("Keyboard Movement")]
    public string axisHorizontal = "Horizontal";
    [SerializeField] string axisVertical = "Vertical";



    [Space, Space, Header("Joystick Movement")]
    [SerializeField] string axislsHorizontal = "JoystickLSHorizontal";
    [SerializeField] string axisLsVertical = "JoystickLSVertical";



    [Space, Space, Header("Keyboard Camera Movement")]
    [SerializeField] string axisCameraHorizontal = "CameraHorizontal";
    [SerializeField] string axisCameraVertical = "CameraVertical";



    [Space, Space, Header("Joystick Camera Movement")]
    [SerializeField] string axisRsHorizontal = "JoystickRSHorizontal";
    [SerializeField] string axisRsVertical = "JoystickRSVertical";



    [Space, Space, Header("Joystick Others")]
    [SerializeField] float joystickInputNoise = .1f;



    [Space, Space, Header("Combat")]
    [SerializeField] KeyCode keyAttack = KeyCode.Mouse0;
    [SerializeField] KeyCode keyJoystickAttack = KeyCode.JoystickButton0;


    [Space, Space, Header("Interaction With Items")]
    [SerializeField] KeyCode keyMainInteraction = KeyCode.Space;
    [SerializeField] KeyCode keyJoystickMainInteraction = KeyCode.JoystickButton1;

    [Space]
    [SerializeField] KeyCode keySecondaryInteraction = KeyCode.Q;
    [SerializeField] KeyCode keyJoystickSecondaryInteraction = KeyCode.JoystickButton3;


    [Space, Space, Header("Dialogs")]
    [SerializeField] KeyCode KeySkip = KeyCode.Escape;
    [SerializeField] KeyCode keyJoystickSkip = KeyCode.JoystickButton7;

    [Space]
    [SerializeField] KeyCode KeyProgressDialog = KeyCode.Space;
    [SerializeField] KeyCode keyJoystickProgressDialog = KeyCode.JoystickButton0;


}
