﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuCanvasManager : MonoBehaviour
{
    [SerializeField] Image fadeImage;
    [SerializeField] float fadeDelay = .25f;
    [SerializeField] bool fadeIn;
    [SerializeField] bool fadeOut;


    void Start()
    {

        if (fadeIn)
            StartCoroutine(HideFadeImage(fadeDelay));
    }

    IEnumerator HideFadeImage(float delay)
    {
        if (!fadeImage)
            yield break;

        fadeImage.CrossFadeAlpha(1, 0, true);
        fadeImage.gameObject.SetActive(true);

        fadeImage.CrossFadeAlpha(0, Mathf.Max(Time.deltaTime, fadeDelay), true);
        yield return new WaitForSeconds(Mathf.Max(Time.deltaTime, fadeDelay) + 0.1f);
        fadeImage.gameObject.SetActive(false);

    }
    IEnumerator ShowFadeImage(float delay,Action action = null)
    {
        if (!fadeImage)
            yield break;

        fadeImage.CrossFadeAlpha(0, 0, true);
        fadeImage.gameObject.SetActive(true);

        fadeImage.CrossFadeAlpha(1f, Mathf.Max(Time.deltaTime, fadeDelay), true);
        yield return new WaitForSeconds(Mathf.Max(Time.deltaTime, fadeDelay) + 0.1f);
        if(action!= null)
            action.Invoke();
    }

    public void LoadLevel(string loadLevel)
    {
        if(!fadeOut)
            SceneManager.LoadScene(loadLevel);
        else
        {
            StartCoroutine(ShowFadeImage(fadeDelay, () => { SceneManager.LoadScene(loadLevel); }));
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }

}