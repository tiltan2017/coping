﻿using System;
using UnityEngine;

[Serializable]


public struct NoteMessage
{
    public static void ShowNote(NoteMessage note)
    {
        if(note.showNote)
            DialogManager.Instance.ShowNote(note.text, note.showBackground, note.noteDuration);
    }
    public void ShowNote()
    {
        if (showNote)
            DialogManager.Instance.ShowNote(text, showBackground, noteDuration);
    }

    public bool showNote;
    public bool showBackground;
    public string text;

    [Tooltip("if set to 0 or a negative number, a default value is used instead")]
    public float noteDuration;
}