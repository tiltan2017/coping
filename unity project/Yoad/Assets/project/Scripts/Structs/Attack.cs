using System;
using UnityEngine;



interface IAttack
{
    void Affect(ILivingThing Other);
}

[Serializable]
public class AttackEffect : IAttack
{
    [Header("Attack Effect")]
    public int damage;

    [Space]
    bool modifySpeed;
    int SpeedModificationPrecentage;
    int SpeedModificationDuration;

    [Space]
    bool stunEffect;
    float stunDuration;

    [Space]
    public bool knockBackEnemy;
    public float enemyKnockBackForce;
    public float enemyUpwardsKnockBackForce;


    public virtual void Affect(ILivingThing other)
    {
        if (damage != 0)
            other.RecieveDamage(damage);

        if(modifySpeed)
            other.ModifySpeed(SpeedModificationPrecentage, SpeedModificationDuration);

        if(stunEffect)
            other.Stun(stunDuration);

        if(knockBackEnemy)
            other.ApplyLocalForce(Vector3.back*enemyKnockBackForce + Vector3.up*enemyUpwardsKnockBackForce,
                enemyUpwardsKnockBackForce);


    }
}

[Serializable]
public class MultiCollisionMeleeAttack : AttackEffect
{


    [Space, Space, Header("Multi Collision Melee Attack")]
    public string animatationTransactionName; // refers a boolean variable
    public float delayBeforeNextAttack;
    public float resetComboAfterDelay;


    public float pushPlayerForwardAfterDelay;
    public float pushPlayerForwardForce;
    public float hitEnemyAfterDelay;
}