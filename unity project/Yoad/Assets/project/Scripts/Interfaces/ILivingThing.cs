﻿using System;
using UnityEngine;
using System.Collections;



public interface ILivingThing : IMovement,ICombat  {


    new GameObject Instance { get; }


    bool IsInvincible { get; set; }


    int MaxHP { get; set; }
    int CurrentHP { get; set; }


    void RecieveDamage(int damage);


    new Action<ILivingThing> OnSpeedIncreased { get; set; }
    new Action<ILivingThing> OnSpeedDecreased { get; set; }
    new Action<ILivingThing> OnSpeedModifiedStart { get; set; }
    new Action<ILivingThing> OnSpeedModifiedEnd { get; set; }
    new Action<ILivingThing> OnStunStart { get; set; }
    new Action<ILivingThing> OnStunEnd { get; set; }

}



