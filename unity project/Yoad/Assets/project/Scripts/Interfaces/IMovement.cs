using System;
using UnityEngine;

public interface IMovement
{

    GameObject Instance { get; }

    //-----------------------------------------------

    /// <summary>
    /// a property that enables/disables the movement of the script user
    /// </summary>
    bool IsEnabled { set; get; }

    /// <summary>
    /// true, if the movement is locked
    /// </summary>
    bool IsMovementInputsLocked { get; }

    /// <summary>
    /// if true, all methods that modify/affect the movement are ignored
    /// </summary>
    bool IsAffectableByOutsideForces { get; set; }

    /// <summary>
    /// returns true if the speed is modified
    /// </summary>
    bool IsSpeedModified { get; }

    //-----------------------------------------------


    /// <summary>
    /// returns or sets the running speed of the user
    /// </summary>
    /// 
    float RunningSpeed { get; set; }

    /// <summary>
    ///returns or sets the walking speed of the user
    /// </summary>
    float WalkingSpeed { get; set; }

    /// <summary>
    /// returns true if the player is moving using inputs
    /// (returns false if the movement is locked)
    /// </summary>
    bool IsPlayerMoving { get; }


    //-----------------------------------------------

    /// <summary>
    /// locks the player from moving. ( ignores the movement inputs ).
    /// the method also resets the "LockMovement(float delaySeconds)" method
    /// </summary>
    /// <param name="state"> if true, the movement gets locked, otherwise it's not </param>
    void LockMovementInputs(bool state);

    /// <summary>
    /// locked movement for a certain amount of seconds.
    /// if movement is already locked, the call is ignored
    /// </summary>
    /// <param name="delaySeconds">the effect duration</param>
    void LockMovement(float delaySeconds);

    //-----------------------------------------------

    /// <summary>
    /// modifies (increases/decreases) the speed of the user by a given percentage, for a certain amount of seconds.
    /// </summary>
    /// <param name="affectPercentage">the modification percentage</param>
    /// <param name="delaySeconds">the effect duration</param>
    void ModifySpeed(int affectPercentage, float delaySeconds);

    /// <summary>
    /// applies force in a word space direction
    /// </summary>
    /// <param name="direction">the direction in which the force is applied</param>
    /// <param name="force">the magnitude of the force</param>
    void ApplyForce(Vector3 direction, float force);

    /// <summary>
    /// applies force in a local direction relative to the mesh
    /// </summary>
    /// <param name="direction">the direction in which the force is applied</param>
    /// <param name="force">the magnitude of the force</param>
    void ApplyLocalForce(Vector3 direction, float force);

    //-----------------------------------------------
    /// <summary>
    /// lock player movement and play stun animation.
    /// </summary>
    /// <param name="delaySeconds">stun duration in seconds</param>
    void Stun(float delaySeconds);

    /// <summary>
    /// resets the walking and running speed of the player to their initial values
    /// </summary>
    void ResetSpeed();

    //-----------------------------------------------

    void MoveUpdate();
 

    //events-----------------------------------------
    Action OnSpeedIncreased { get; set; }
    Action OnSpeedDecreased { get; set; }
    Action OnSpeedModifiedStart { get; set; }
    Action OnSpeedModifiedEnd { get; set; }
    Action OnStunStart { get; set; }
    Action OnStunEnd { get; set; }
}