﻿using System;
using UnityEngine;
using System.Collections;
using System.Text;


public interface IInteractiveItem
{

 
    
    //---------------------------------------
    GameObject Instance { get; }
    bool IsDestryed { get; }
    //---------------------------------------
    ExternalCollider ExternalCollider { get; }
    //---------------------------------------
    Action OnMainInteraction { get; set; }
    Action OnSecondaryInteraction { get; set; }
    //-----------------------
    void MainInteraction();
    void SeconderyInteraction();
    //---------------------------------------
    bool IsGuiVisable { get;  }
    bool IsGuiForcedHidden { get; }
    bool IsGuiForcedVisable { get; }
    bool IsGuiAlwaysVisable { get; }
    bool IsGuiAlwaysHidden { get; }
    //-----------------------
    void HideGUI();
    void ShowGUI();
    void ForceShowGui(bool state);
    void ForceHideGui(bool state);
    //---------------------------------------
    bool IsInteractionDiabled { get; }
    //-----------------------
    void DisableInteraction(bool state);
    //---------------------------------------

}



