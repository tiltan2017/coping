using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public interface ICombat
{
    GameObject Instance { get; }
    bool IsEnabled { get; set; }
    bool IsCombatLocked{ get; set; }

    List<string> TargetsTags { get; }


    void LockCombat(float delaySeconds);



    void Initialize(IMovement movement);
    void Attack();
    

    Action onAttackStart { get; set; }
    Action onAttackEnd { get; set; }
    Action<ILivingThing> onAttackHit { get; set; }



}


