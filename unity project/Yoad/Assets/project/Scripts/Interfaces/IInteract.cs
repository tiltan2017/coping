﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public interface IInteractor
{
    GameObject Instance { get; }

    List<IInteractiveItem> ItemsInRange {get;}
    IInteractiveItem NearestItem { get; }
    void AddItem(IInteractiveItem item);
    void RemoveItem(IInteractiveItem item);




    void IteractWithItem(bool isMainInteraction);

}
